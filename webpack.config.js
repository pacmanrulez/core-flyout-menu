/*
 *     Date: 2023
 *  Package: core-flyout-menu
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-flyout-menu
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-flyout-menu.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-flyout-menu.js',
    library: "coreFlyoutMenu",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};

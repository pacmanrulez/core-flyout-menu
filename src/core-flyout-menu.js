/*
 *     Date: 2023
 *  Package: core-flyout-menu
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-flyout-menu
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    if(!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        PACKAGE = {         // package description
            name: 'Flyout'
        },
        DOM = require('core-utilities/utility/dom'),
        Package = require('core-utilities/utility/package'),

        // default options
        DEFAULT_OPTIONS = {},

    Flyout = function(name, options) {
        options = {
            ...DEFAULT_OPTIONS,
            ...options
        };
    };

    Flyout.inst = function(name, options) {
        return new PACKAGE.Flyout(name, options);
    }

    PACKAGE.Flyout = Flyout;
    // assign to global object core
    return Package.return(PACKAGE);
})();